
//var botanio = require('botanio')('LfUV3QydCes6dT0xU_cGOJS_2WBneAdG');

//test
var tg = require('telegram-node-bot')('213802446:AAFsICeqgD5_inbldOY-UEO6GphRl8cPOjc');

var shuffle = (str) => str.split('').sort( () => 0.5 - Math.random()).join(''),
    processMessage = (msg) => {
        if (msg)
             return msg.split(/\s+/g).map(processSingle).join(' ');

        return msg || '';
    },
    processSingle = msg => {
        if (msg.length > 3) {
            var first = msg[0];
            msg = msg.substr(1, msg.length);

            var last = msg[msg.length - 1];
            msg = msg.slice(0, msg.length - 1);

            msg = first + shuffle(msg) + last;
        }

        return msg || '';
    };

tg.router.when(['/start'], 'MainController')
    .otherwise("MainController");

tg.controller('MainController', ($) => {

    var msg = processMessage($.args);
    $.sendMessage(msg);
});

tg.inlineMode($ => {
    console.log('Incoming message=' + $.query);
    var msg = processMessage($.query);
    console.log('Parsed messg=' + msg);

    tg.answerInlineQuery($.id, [{
        type: 'article',
        title: msg,
        input_message_content: {
            message_text: msg,
            parse_mode: 'Markdown'
        }
    }],  (a, err) => {
        if (err) console.error(err);
    });
});
